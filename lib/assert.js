/**
 * Comonly used asssertions for strict mode
 */
define(function() {
    'use strict';
    var exports = {};


    function scanCollection(__collection, __cb, __compare) {
        var _i;

        for (_i = 0; _i < __collection.length; _i += 1) {
            if (__cb(__collection[_i]) === __compare) {
                return true;
            }
        }
        return false;
    }

    /**
     * Evaluates value to be a number int|float
     * @param   {Array} __array assert value
     * @returns {Boolean}  true is int|float, false on anything else
     */
    function numberArray(__array) {
        if (!array(__array)) {
            return false;
        }
        return  scanCollection(__array, number, false) !== true;
    }
    exports.numberArray = numberArray;

    /**
     * Evaluates value to be a number int|float
     * @param   {Array} __args assert value
     * @returns {Boolean}  true is int|float, false on anything else
     */
    function numberArgs(__args) {
        if (!__args || !number(__args.length)) {
            return false;
        }
        return  scanCollection(__args, number, false) !== true;
    }
    exports.numberArgs = numberArgs;






    /**
     * Evaluates value to be a number int|float
     * @param   {Mixed} __value assert value
     * @returns {Boolean}  true is int|float, false on anything else
     */
    function number(__value) {
        return typeof __value === 'number' && Math.abs(__value) !== Infinity;
    }
    exports.number = number;

    /**
     * Evaluates value to be a number int|float or null or undefined
     * @param   {Mixed} __value assert value
     * @returns {Boolean}  true is int|float or null or undefined, false on anything else
     */
    function optNumber(__value) {
        return noru(__value) || number(__value);
    }
    exports.optNumber = optNumber;

    /**
     * Evaluates value to be a positive int|float
     * @param   {Mixed} __value assert value
     * @returns {Boolean}  true is int|float more than 0, false on anything else
     */
    function positive(__value) {
        return number(__value) && __value > 0;
    }
    exports.positive = positive;

    /**
     * Evaluates value to be a positive int|float or null|undefined
     * @param   {Mixed} __value assert value
     * @returns {Boolean}  true if int|float and more than 0 or or null|undefined, false on anything else
     */
    function optPositive(__value) {
        return noru(__value) || positive(__value);
    }
    exports.optPositive = optPositive;

    /**
     * Evaluates value to be a negative int|float
     * @param   {Mixed} __value assert value
     * @returns {Boolean}  true if int|float less than 0, false on anything else
     */
    function negative(__value) {
        return number(__value) && __value < 0;
    }
    exports.negative = negative;

    /**
     * Evaluates value to be a negative int|float or or null|undefined
     * @param   {Mixed} __value assert value
     * @returns {Boolean}  true is int|float less than 0, or null|undefined, false on anything else
     */
    function optNegative(__value) {
        return noru(__value) || negative(__value);
    }
    exports.optNegative = optNegative;

    /**
     * Evaluates value to be a zero
     * @param   {Mixed} __value assert value
     * @returns {Boolean}  true is int|float equals 0, false on anything else
     */
    function zero(__value) {
        return number(__value) && __value === 0;
    }
    exports.zero = zero;

    /**
     * Evaluates value to be Int
     * @param   {Mixed} __value assert value
     * @returns {Boolean}  true if int or 0.0 - 0.0++0, false on anything else
     */
    function int(__value) {
        return Number.isInteger(__value);
    }
    exports.int = int;

    /**
     * Evaluates value to be Integer and more or equal zero
     * @param   {Mixed} __value assert value
     * @returns {Boolean}  true if int and >= 0, false on anything else
     */
    function index(__value) {
        return int(__value) && __value >= 0;
    }
    exports.index = index;

    /**
     * Evaluates value to be Int or null|undefined
     * @param   {Mixed} __value assert value
     * @returns {Boolean}  true if int or 0.0 - 0.0++0 or null|undefined, false on anything else
     */
    function optInt(__value) {
        return noru(__value) || int(__value);
    }
    exports.optInt = optInt;

    /**
     * Evaluates value to be a positive Intature
     * @param   {Mixed} __value assert value
     * @returns {Boolean}  true if int or 0.0 - 0.0++0 and > 0, false on anything else
     */
    function pint(__value) {
        return int(__value) && __value > 0;
    }
    exports.pint = pint;

    /**
     * Evaluates value to be a positive Intature or null|undefined
     * @param   {Mixed} __value assert value
     * @returns {Boolean}  true if int or 0.0 - 0.0++0 and > 0, or null|undefined, false on anything else
     */
    function optPint(__value) {
        return noru(__value) || pint(__value);
    }
    exports.optPint = optPint;

    /**
     * Evaluates value to be a negative Intature
     * @param   {Mixed} __value assert value
     * @returns {Boolean}  true if int or 0.0 - 0.0++0 and > 0, false on anything else
     */
    function nint(__value) {
        return int(__value) && __value < 0;
    }
    exports.nint = nint;

    /**
     * Evaluates value to be a negative Intature or null|undefined
     * @param   {Mixed} __value assert value
     * @returns {Boolean}  true if int or 0.0 - 0.0++0 and < 0, or null|undefined, false on anything else
     */
    function optNint(__value) {
        return noru(__value) || nint(__value);
    }
    exports.optNint = optNint;

    /**
     * Evaluates value to be float
     * @param   {Mixed} __value assert value
     * @returns {Boolean}  true if float, false on 0.0 - 0.0++0 and anything else
     */
    function float(__value) {
        return number(__value) && !Number.isInteger(__value);
    }
    exports.float = float;

    /**
     * Evaluates value to be a float or null|undefined
     * @param   {Mixed} __value assert value
     * @returns {Boolean}  true if float or null|undefined, false on 0.0 - 0.0++0 and anything else
     */
    function optFloat(__value) {
        return noru(__value) || float(__value);
    }
    exports.optFloat = optFloat;

    /**
     * Evaluates value to be a positive float
     * @param   {Mixed} __value assert value
     * @returns {Boolean}  true if float > 0, false on 0.0 - 0.0++0 and anything else
     */
    function pfloat(__value) {
        return float(__value) && __value > 0;
    }
    exports.pfloat = pfloat;

    /**
     * Evaluates value to be a positive float or null|undefined
     * @param   {Mixed} __value assert value
     * @returns {Boolean}  true if float > 0 or null|undefined, false on 0.0 - 0.0++0 and anything else
     */
    function optPfloat(__value) {
        return noru(__value) || pfloat(__value);
    }
    exports.optPfloat = optPfloat;

    /**
     * Evaluates value to be a negative positive float
     * @param   {Mixed} __value assert value
     * @returns {Boolean}  true if float < 0, false on 0.0 - 0.0++0 and anything else
     */
    function nfloat(__value) {
        return float(__value) && __value < 0;
    }
    exports.nfloat = nfloat;

    /**
     * Evaluates value to be a negative float or null|undefined
     * @param   {Mixed} __value assert value
     * @returns {Boolean}  true if float < 0 or null|undefined, false on 0.0 - 0.0++0 and anything else
     */
    function optNfloat(__value) {
        return noru(__value) || nfloat(__value);
    }
    exports.optNfloat = optNfloat;

    /**
     * Evaluates value to be a non empty String after trimming
     * @param   {Mixed} __value assert value
     * @returns {Boolean}  true if string and is not empty, false on anything else
     */
    function string(__value) {
        return typeof __value === 'string' && __value.trim() !== '';
    }
    exports.string = string;

    /**
     * Evaluates value to be a non empty String after trimming or null|undefined
     * @param   {Mixed} __value assert value
     * @returns {Boolean}  true if string and is not empty or null|undefined, false on anything else
     */
    function optString(__value) {
        return noru(__value) || string(__value);
    }
    exports.optString = optString;

    /**
     * Evaluates value to be a non empty String after trimming and have its first character to be in apper case
     * @param   {Mixed} __value assert value
     * @returns {Boolean}  true if string and is not empty & first character upper, false on anything else
     */
    function fustr(__value) {
        return string(__value) && __value.charAt(0) === __value.charAt(0).toUpperCase();
    }
    exports.fustr = fustr;

    /**
     * Evaluates value to be a non empty String after trimming and have its first character to be in apper case
     * @param   {Mixed} __value assert value
     * @returns {Boolean}  true if string and is not empty & first character upper, false on anything else
     */
    function optFustr(__value) {
        return noru(__value) || fustr(__value);
    }
    exports.optFustr = optFustr;

    /**
     * Evaluates value to be a non empty String after trimming and have its first character to be in lower case
     * @param   {Mixed} __value assert value
     * @returns {Boolean}  true if string and is not empty & first character upper, false on anything else
     */
    function flostr(__value) {
        return string(__value) && __value.charAt(0) === __value.charAt(0).toLowerCase();
    }
    exports.flostr = flostr;

    /**
     * Evaluates value to be a non empty String after trimming and have its first character to be in lower case
     * @param   {Mixed} __value assert value
     * @returns {Boolean}  true if string and is not empty & first character upper, false on anything else
     */
    function optFlostr(__value) {
        return noru(__value) || flostr(__value);
    }
    exports.optFlostr = optFlostr;

    /**
     * Evaluates value to be Object [object Object]
     * @param   {Mixed} __value assert value
     * @returns {Boolean}  true if Object[object Object], false on anything else
     */
    function object(__value) {
        return Object.prototype.toString.call(__value) === '[object Object]';
    }
    exports.object = object;

    /**
     * Evaluates value to be Object [object Object]
     * @param   {Mixed} __value assert value
     * @returns {Boolean}  true if Object[object Object], false on anything else
     */
    function optObject(__value) {
        return noru(__value) || object(__value);
    }
    exports.optObject = optObject;

    /**
     * Evaluates value to be Object [object Object] and not empty (has own values)
     * @param   {Mixed} __value assert value
     * @returns {Boolean}  true if non empty Object, false on anything else
     */
    function neobject(__value) {
        return object(__value) && Object.keys(__value).length > 0;
    }
    exports.neobject = neobject;

    /**
     * Evaluates value to be Object [object Object] and not empty (has own values)|null|undefined
     * @param   {Mixed} __value assert value
     * @returns {Boolean}  true if non empty Object|null|undefined, false on anything else
     */
    function optNeobject(__value) {
        return noru(__value) || neobject(__value);
    }
    exports.optNeobject = optNeobject;

    /**
     * Evaluates value to be Empty Object [object Object]
     * @param   {Mixed} __value assert value
     * @returns {Boolean}  true if empty Object, false on anything else
     */
    function eobject(__value) {
        return object(__value) && Object.keys(__value).length === 0;
    }
    exports.eobject = eobject;
    //no need to make amtpy optional

    /**
     * Evaluates value to be Array [object Array]
     * @param   {Mixed} __value assert value
     * @returns {Boolean}  true if Array, false on anything else
     */
    function array(__value) {
        return Array.isArray(__value);
    }
    exports.array = array;

    /**
     * Evaluates value to be Array [object Array] | null|undefined
     * @param   {Mixed} __value assert value
     * @returns {Boolean}  true if Array | null|undefined, false on anything else
     */
    function optArray(__value) {
        return noru(__value) || array(__value);
    }
    exports.optArray = optArray;

    /**
     * Evaluates value to be Float32Array [object Array]
     * @param   {Mixed} __value assert value
     * @returns {Boolean}  true if Float32Array, false on anything else
     */
    function array32(__value) {
        return __value instanceof Float32Array;
    }
    exports.array32 = array32;

    /**
     * Evaluates value to be Array [object Array] | null|undefined
     * @param   {Mixed} __value assert value
     * @returns {Boolean}  true if Array | null|undefined, false on anything else
     */
    function optArray32(__value) {
        return noru(__value) || array32(__value);
    }
    exports.optArray32 = optArray32;

    /**
     * Evaluates value to be Array [object Array] and not empty
     * @param   {Mixed} __value assert value
     * @returns {Boolean}  true if non empty Array, false on anything else
     */
    function nearray(__value) {
        return array(__value) && __value.length > 0;
    }
    exports.nearray = nearray;

    /**
     * Evaluates value to be Array [object Array] and not empty | null|undefined
     * @param   {Mixed} __value assert value
     * @returns {Boolean}  true if non empty Array | null|undefined, false on anything else
     */
    function optNearray(__value) {
        return noru(__value) || nearray(__value);
    }
    exports.optNearray = optNearray;

    /**
     * Evaluates value to be Empty Array [object Array]
     * @param   {Mixed} __value assert value
     * @returns {Boolean}  true if empty Array, false on anything else
     */
    function earray(__value) {
        return array(__value) && __value.length === 0;
    }
    exports.earray = earray;

    /**
     * Evaluates value to be Boolean
     * @param   {Mixed} __value assert value
     * @returns {Boolean}  true if true or false, false on anything else
     */
    function boolean(__value) {
        return typeof __value === 'boolean';
    }
    exports.boolean = boolean;

    /**
     * Evaluates value to be Boolean | null|undefined
     * @param   {Mixed} __value assert value
     * @returns {Boolean}  true if true or false | null|undefined, false on anything else
     */
    function optBoolean(__value) {
        return noru(__value) || boolean(__value);
    }
    exports.optBoolean = optBoolean;

    /**
     * Evaluates value to be True | null|undefined
     * @param   {Mixed} __value assert value
     * @returns {Boolean}  true if true | null|undefined, false on anything else
     */
    function optTrue(__value) {
        return noru(__value) || __value === true;
    }
    exports.optTrue = optTrue;

    /**
     * Evaluates value to be False | null|undefined
     * @param   {Mixed} __value assert value
     * @returns {Boolean}  true if false | null|undefined, false on anything else
     */
    function optFalse(__value) {
        return noru(__value) || __value === false;
    }
    exports.optFalse = optFalse;

    /**
     * Evaluates value to be truthy (defined, not null, more than 0 if a number)
     * @param   {Mixed} __value assert value
     * @returns {Boolean}  true if empty Array, false on anything else
     */
    function truthy(__value) {
        if (noru(__value)) {
            return false;
        }

        if (number(__value)) {
            return positive(__value);
        }

        if (array(__value)) {
            return nearray(__value);
        }

        if (object(__value)) {
            return neobject(__value);
        }

        if (string(__value)) {
            return true;
        }

        if (boolean(__value)) {
            return __value;
        }

        return false;
    }
    exports.truthy = truthy;

    /**
     * Evaluates value to be falsy opposite of truthy
     * @param   {Mixed} __value assert value
     * @returns {Boolean}  true falsy, false on anything else
     */
    function falsy(__value) {
        return !truthy(__value);
    }
    exports.falsy = falsy;

    /**
     * Evaluates value to be null or undefined
     * @param   {Mixed} __value assert value
     * @returns {Boolean}  true if null or undefined, false on anything else
     */
    function noru(__value) {
        return __value == null;
    }
    exports.noru = noru;

    /**
     * Evaluates value to be null or undefined
     * @param   {Mixed} __value assert value
     * @returns {Boolean}  true if null or undefined, false on anything else
     */
    function notnoru(__value) {
        return !noru(__value);
    }
    exports.notnoru = notnoru;

    /**
     * Evaluates value to be undefined
     * @param   {Mixed} __value assert value
     * @returns {Boolean}  true if undefined, false on anything else
     */
    function undef(__value) {
        return __value === undefined;
    }
    exports.undef = undef;

    /**
     * Evaluates value to be defined
     * @param   {Mixed} __value assert value
     * @returns {Boolean}  true if defined, false on anything else
     */
    function def(__value) {
        return !undef(__value);
    }
    exports.def = def;

    /**
     * Evaluates value to be null
     * @param   {Mixed} __value assert value
     * @returns {Boolean}  true if null, false on anything else
     */
    function nul(__value) {
        return __value === null;
    }
    exports.nul = nul;

    /**
     * Evaluates value to not be null
     * @param   {Mixed} __value assert value
     * @returns {Boolean}  true not null, false on anything else
     */
    function notnul(__value) {
        return !nul(__value);
    }
    exports.notnul = notnul;

    /**
     * Evaluates value to be function
     * @param   {Mixed} __value assert value
     * @returns {Boolean}  true function, false on anything else
     */
    function func(__value) {
        return typeof __value === 'function';
    }
    exports.func = func;

    /**
     * Evaluates value to be function | null|undefined
     * @param   {Mixed} __value assert value
     * @returns {Boolean}  true function | null|undefined, false on anything else
     */
    function optFunc(__value) {
        return noru(__value) || func(__value);
    }
    exports.optFunc = optFunc;

    /**
     * Evaluates value to be a valid hash number > 0|string non empty
     * @param   {Mixed} __value assert value
     * @returns {Boolean}  true number|string, false on anything else
     */
    function hash(__value) {
        return positive(__value) || string(__value);
    }
    exports.hash = hash;

    /**
     * Evaluates value to be a valid hash number > 0|string non empty
     * @param   {Mixed} __value assert value
     * @returns {Boolean}  true number|string, false on anything else
     */
    function optHash(__value) {
        return noru(__value) || hash(__value);
    }
    exports.optHash = optHash;

    return exports;
});
