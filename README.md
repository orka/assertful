## ASSERTFUL
Assert libriary


* intended to help assert commonly needed tests

### number([1])
***
Evaluates argument to be a number int|float
+ **@param   {Mixed}**  asserted argument
+ **@returns {Boolean}**  true is int|float, false on anything else

### optNumber([1])
***
Evaluates argument to be a number int|float or null or undefined
+ **@param   {Mixed}**  asserted argument
+ **@returns {Boolean}**  true is int|float or null or undefined, false on anything else

### positive([1])
***
Evaluates argument to be a positive int|float
+ **@param   {Mixed}**  asserted argument
+ **@returns {Boolean}**  true if int|float and more than 0, false on anything else

### optPositive([1])
***
Evaluates argument to be a positive int|float or null|undefined
+ **@param   {Mixed}**  asserted argument
+ **@returns {Boolean}**  true if int|float and more than 0, or null|undefined, false on anything else

### negative([1])
***
Evaluates argument to be a negative int|float
+ **@param   {Mixed}**  asserted argument
- **@returns {Boolean}**  true if int|float and less than 0, false on anything else

### optNegative([1])
***
Evaluates argument to be a negative int|float, or null|undefined,
+ **@param   {Mixed}**  asserted argument
- **@returns {Boolean}**  true if int|float and less than 0, or null|undefined,, false on anything else

### zero([1])
***
Evaluates argument to be a zero
+ **@param   {Mixed}**  asserted argument
- **@returns {Boolean}**  true if int|float and equals 0, false on anything else

### int([1])
***
Evaluates argument to be an intature
+ **@param   {Mixed}**  asserted argument
- **@returns {Boolean}**  true if int|0.0|0.0++0, false on anything else

### optInt([1])
***
Evaluates argument to be an intature or null|undefined
+ **@param   {Mixed}**  asserted argument
- **@returns {Boolean}**  true if int|0.0|0.0++0 or null|undefined, false on anything else

### pint([1])
***
Evaluates value to be a positive Intature
+ **@param   {Mixed}**  asserted argument
- **@returns {Boolean}**  true if int or 0.0 - 0.0++0 and > 0, false on anything else

### optPint([1])
***
Evaluates value to be a positive Intature or null|undefined
+ **@param   {Mixed}**  asserted argument
- **@returns {Boolean}**  true if int or 0.0 - 0.0++0 and > 0, or null|undefined, false on anything else

### nint([1])
***
Evaluates value to be a negative Intature
+ **@param   {Mixed}**  asserted argument
- **@returns {Boolean}**  true if int or 0.0 - 0.0++0 and < 0, false on anything else

### optNint([1])
***
Evaluates value to be a negative Intature or null|undefined
+ **@param   {Mixed}**  asserted argument
- **@returns {Boolean}**  true if int or 0.0 - 0.0++0 and < 0, or null|undefined, false on anything else

### float([1])
***
Evaluates argument to be a float
+ **@param   {Mixed}**  asserted argument
- **@returns {Boolean}**  true if float, false on 0.0|0.0++0|anything else

### optFloat([1])
***
Evaluates argument to be a float or null|undefined
+ **@param   {Mixed}**  asserted argument
- **@returns {Boolean}**  true if float or null|undefined, false on 0.0|0.0++0|anything else

### pfloat([1])
***
Evaluates argument to be a positive float
+ **@param   {Mixed}**  asserted argument
- **@returns {Boolean}**  true if float > 0, false on 0.0|0.0++0|anything else

### optPfloat([1])
***
Evaluates argument to be a positive float or null|undefined
+ **@param   {Mixed}**  asserted argument
- **@returns {Boolean}**  true if float > 0 or null|undefined, false on 0.0|0.0++0|anything else

### nfloat([1])
***
Evaluates argument to be a negative float
+ **@param   {Mixed}**  asserted argument
- **@returns {Boolean}**  true if float < 0, false on 0.0|0.0++0|anything else

### optNfloat([1])
***
Evaluates argument to be a negative float or null|undefined
+ **@param   {Mixed}**  asserted argument
- **@returns {Boolean}**  true if float < 0 or null|undefined, false on 0.0|0.0++0|anything else

### string([1])
***
Evaluates argument to be a non empty string
+ **@param   {Mixed}**  asserted argument
- **@returns {Boolean}** true if string and is not empty, false on anything else

### optString([1])
***
Evaluates argument to be a non empty string or null|undefined
+ **@param   {Mixed}**  asserted argument
- **@returns {Boolean}** true if string and is not empty, or null|undefined, false on anything else

### fustr([1])
***
Evaluates argument to be a non empty string and have its first character to be in apper case
+ **@param   {Mixed}**  asserted argument
- **@returns {Boolean}** true if string and is not empty and first upper, false on anything else

### optFustr([1])
***
Evaluates argument to be a non empty string and have its first character to be in apper case | null|undefined
+ **@param   {Mixed}**  asserted argument
- **@returns {Boolean}** true if string and is not empty and first upper null|undefined, false on anything else

### flostr([1])
***
Evaluates argument to be a non empty string and have its first character to be in lower case
+ **@param   {Mixed}**  asserted argument
- **@returns {Boolean}** true if string and is not empty and first lower, false on anything else

### optFlostr([1])
***
Evaluates argument to be a non empty string and have its first character to be in lower case | null|undefined
+ **@param   {Mixed}**  asserted argument
- **@returns {Boolean}** true if string and is not empty and first lower null|undefined, false on anything else

### object([1])
***
Evaluates argument to be an object
+ **@param   {Mixed}**  asserted argument
- **@returns {Boolean}** true if Object[object Object], false on anything else

### optObject([1])
***
Evaluates argument to be an object|null|undefined
+ **@param   {Mixed}**  asserted argument
- **@returns {Boolean}** true if Object[object Object]|null|undefined, false on anything else

### neobject([1])
***
Evaluates argument to be a non empty object, having own properties
+ **@param   {Mixed}**  asserted argument
- **@returns {Boolean}** true if non empty Object, false on anything else

### optNeobject([1])
***
Recomended!
Evaluates argument to be a non empty object, having own properties | null|undefined
+ **@param   {Mixed}**  asserted argument
- **@returns {Boolean}** true if non empty Object | null|undefined, false on anything else


### eobject([1])
***
Evaluates argument to be an empty object, not having own properties
+ **@param   {Mixed}**  asserted argument
- **@returns {Boolean}** true if empty Object, false on anything else

### array([1])
***
Evaluates argument to be an array
+ **@param   {Mixed}**  asserted argument
- **@returns {Boolean}** true if any array, false on anything else

### optArray([1])
***
Evaluates argument to be an array | null|undefined
+ **@param   {Mixed}**  asserted argument
- **@returns {Boolean}** true if any array | null|undefined, false on anything else

### nearray([1])
***
Evaluates argument to be a non empty array, length > 0
+ **@param   {Mixed}**  asserted argument
- **@returns {Boolean}** true if non empty Array, false on anything else

### optNearray([1])
***
Recomended!
Evaluates argument to be a non empty array, length > 0 | null|undefined
+ **@param   {Mixed}**  asserted argument
- **@returns {Boolean}** true if non empty Array | null|undefined, false on anything else

### earray([1])
***
Evaluates argument to be an empty array, length === 0
+ **@param   {Mixed}**  asserted argument
- **@returns {Boolean}** true if empty Array, false on anything else

### boolean([1])
***
Evaluates argument to be a boolean true|false
+ **@param   {Mixed}**  asserted argument
- **@returns {Boolean}** true if true|false, false on anything else

### optBoolean([1])
***
Recomended
Evaluates argument to be a boolean true|false | null|undefined
+ **@param   {Mixed}**  asserted argument
- **@returns {Boolean}** true if true|false | null|undefined, false on anything else

### optTrue([1])
***
Recomended
Evaluates argument to be a boolean true | null|undefined
+ **@param   {Mixed}**  asserted argument
- **@returns {Boolean}** true if true | null|undefined, false on anything else

### optFalse([1])
***
Recomended
Evaluates argument to be a boolean false | null|undefined
+ **@param   {Mixed}**  asserted argument
- **@returns {Boolean}** true if false | null|undefined, false on anything else

### truthy([1])
***
Evaluates argument to be truthy
+ **@param   {Mixed}**  asserted argument
- **@returns {Boolean}** true if defined|positive|nearray|object|string|true, false on anything else

### falsy([1])
***
Evaluates argument to be falsy (opposite of truthy)
+ **@param   {Mixed}**  asserted argument
- **@returns {Boolean}** true if !-defined|positive|nearray|object|string|true, false on anything else

### noru([1])
***
Evaluates argument to be null|undefined
+ **@param   {Mixed}**  asserted argument
- **@returns {Boolean}** true if null|undefined, false on anything else

### notnoru([1])
***
Evaluates argument not to be null|undefined
+ **@param   {Mixed}**  asserted argument
- **@returns {Boolean}** true if not null|undefined, false on anything else

### undef([1])
***
Evaluates argument to be undefined
+ **@param   {Mixed}**  asserted argument
- **@returns {Boolean}** true if undefined, false on anything else

### def([1])
***
Evaluates argument to be defined
+ **@param   {Mixed}**  asserted argument
- **@returns {Boolean}** true if defined, false on anything else

### nul([1])
***
Evaluates argument to be null
+ **@param   {Mixed}**  asserted argument
- **@returns {Boolean}** true if null, false on anything else

### notnul([1])
***
Evaluates argument to not be null
+ **@param   {Mixed}**  asserted argument
- **@returns {Boolean}** true if !null, false on anything else

### func([1])
***
Evaluates argument be a function
+ **@param   {Mixed}**  asserted argument
- **@returns {Boolean}** true if function, false on anything else

### func([1])
***
Evaluates argument be a function | null|undefined
+ **@param   {Mixed}**  asserted argument
- **@returns {Boolean}** true if function | null|undefined, false on anything else

### hash([1])
***
Evaluates argument be a valid hash >0|nont empty string
+ **@param   {Mixed}**  asserted argument
- **@returns {Boolean}** true number|string, false on anything else
